
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <xcb/xcb.h>
#define VK_USE_PLATFORM_XCB_KHR
#include <vulkan/vulkan.h>

#define SWAPCHAIN_IMAGES 2
#define WINDOW_W 600
#define WINDOW_H 600

const char *instExtensionNames[] = {VK_KHR_XCB_SURFACE_EXTENSION_NAME,
                                    VK_KHR_SURFACE_EXTENSION_NAME,
                                    VK_EXT_DEBUG_REPORT_EXTENSION_NAME};
unsigned instExtensionCount = 3;

const char *devExtensionNames[] = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};
unsigned devExtensionCount = 1;

const char *instLayerNames[] = {"VK_LAYER_LUNARG_mem_tracker",
                                "VK_LAYER_GOOGLE_unique_objects"};
unsigned instLayerCount = 2;

const char *devLayerNames[] = {"VK_LAYER_LUNARG_mem_tracker",
                               "VK_LAYER_GOOGLE_unique_objects"};
unsigned devLayerCount = 2;

// clang-format off
const float vb[3][3] = {
	{ -0.5f, -0.5f,  0.0f },
	{  0.5f, -0.5f,  0.0f },
	{  0.0f,  0.36f,  0.0f  },
};
// clang-format on

struct sys {
  xcb_connection_t *connection;
  xcb_screen_t *screen;
  xcb_window_t window;
  xcb_intern_atom_reply_t *atom_wm_delete_window;
};

char *read_spv(const char *filename, size_t *psize) {
  long int size;
  void *shader_code;
  size_t retVal;

  FILE *fp = fopen(filename, "rb");
  if (!fp)
    return NULL;

  fseek(fp, 0L, SEEK_END);
  size = ftell(fp);

  fseek(fp, 0L, SEEK_SET);

  shader_code = malloc(size);
  retVal = fread(shader_code, size, 1, fp);
  if (!retVal)
    return NULL;

  *psize = size;

  fclose(fp);
  return shader_code;
}

static int memtype_from_properties(VkPhysicalDeviceMemoryProperties *mempros,
                                   uint32_t typeBits, VkFlags requirements_mask,
                                   uint32_t *typeIndex) {
  for (uint32_t i = 0; i < sizeof(typeBits) * 8; i++) {
    if ((typeBits & 1) == 1) {
      if ((mempros->memoryTypes[i].propertyFlags & requirements_mask) ==
          requirements_mask) {
        *typeIndex = i;
        return VK_TRUE;
      }
    }
    typeBits >>= 1;
  }
  return VK_FALSE;
}

VKAPI_ATTR VkBool32 VKAPI_CALL dbgFunc(VkFlags msgFlags,
                                       VkDebugReportObjectTypeEXT objType,
                                       uint64_t srcObject, size_t location,
                                       int32_t msgCode,
                                       const char *pLayerPrefix,
                                       const char *pMsg, void *pUserData) {
  char *message = (char *)malloc(strlen(pMsg) + 100);

  assert(message);

  if (msgFlags & VK_DEBUG_REPORT_ERROR_BIT_EXT) {
    sprintf(message, "ERROR: [%s] Code %d : %s", pLayerPrefix, msgCode, pMsg);
  } else if (msgFlags & VK_DEBUG_REPORT_WARNING_BIT_EXT) {
    sprintf(message, "WARNING: [%s] Code %d : %s", pLayerPrefix, msgCode, pMsg);
  } else {
    return VK_FALSE;
  }

  printf("%s\n", message);
  fflush(stdout);
  free(message);

  /*
   * false indicates that layer should not bail-out of an
   * API call that had validation failures. This may mean that the
   * app dies inside the driver due to invalid parameter(s).
   * That's what would happen without validation layers, so we'll
   * keep that behavior here.
   */
  return VK_FALSE;
}

static int create_connection(struct sys *sys) {
  const xcb_setup_t *setup;
  xcb_screen_iterator_t iter;
  int scr;

  sys->connection = xcb_connect(NULL, &scr);
  if (sys->connection == NULL) {
    return -1;
  }

  setup = xcb_get_setup(sys->connection);
  iter = xcb_setup_roots_iterator(setup);
  while (scr-- > 0)
    xcb_screen_next(&iter);

  sys->screen = iter.data;
  return 0;
}
static void destroy_connection(struct sys *sys) {
  xcb_disconnect(sys->connection);
  free(sys->atom_wm_delete_window);
}
static void create_window(struct sys *sys) {
  uint32_t value_mask, value_list[32];

  sys->window = xcb_generate_id(sys->connection);

  value_mask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
  value_list[0] = sys->screen->black_pixel;
  value_list[1] = XCB_EVENT_MASK_KEY_RELEASE | XCB_EVENT_MASK_EXPOSURE |
                  XCB_EVENT_MASK_STRUCTURE_NOTIFY;

  xcb_create_window(sys->connection, XCB_COPY_FROM_PARENT, sys->window,
                    sys->screen->root, 0, 0, WINDOW_W, WINDOW_H, 0,
                    XCB_WINDOW_CLASS_INPUT_OUTPUT, sys->screen->root_visual,
                    value_mask, value_list);

  /* Magic code that will send notification when window is destroyed */
  xcb_intern_atom_cookie_t cookie =
      xcb_intern_atom(sys->connection, 1, 12, "WM_PROTOCOLS");
  xcb_intern_atom_reply_t *reply =
      xcb_intern_atom_reply(sys->connection, cookie, 0);

  xcb_intern_atom_cookie_t cookie2 =
      xcb_intern_atom(sys->connection, 0, 16, "WM_DELETE_WINDOW");
  sys->atom_wm_delete_window =
      xcb_intern_atom_reply(sys->connection, cookie2, 0);

  xcb_change_property(sys->connection, XCB_PROP_MODE_REPLACE, sys->window,
                      (*reply).atom, 4, 32, 1,
                      &(*sys->atom_wm_delete_window).atom);
  free(reply);

  xcb_map_window(sys->connection, sys->window);
}
static void destroy_window(struct sys *sys) {
  xcb_destroy_window(sys->connection, sys->window);
}

int main(int argc, char **argv) {
  int i, match;
  VkResult res;

  unsigned ext_count;
  res = vkEnumerateInstanceExtensionProperties(NULL, &ext_count, NULL);
  assert(!res);

  if (ext_count > 0) {
    VkExtensionProperties *exts =
        malloc(ext_count * sizeof(VkExtensionProperties));
    assert(exts);
    res = vkEnumerateInstanceExtensionProperties(NULL, &ext_count, exts);
    assert(!res);
    printf("Available extenstions:\n");
    for (i = 0; i < ext_count; ++i) {
      printf("%s\n", exts[i].extensionName);
    }
    free(exts);
  } else {
    printf("No extenstion available\n");
  }

  unsigned instlayer_count;
  res = vkEnumerateInstanceLayerProperties(&instlayer_count, NULL);
  assert(!res);

  if (instlayer_count > 0) {
    VkLayerProperties *instlayers =
        malloc(sizeof(VkLayerProperties) * instlayer_count);
    res = vkEnumerateInstanceLayerProperties(&instlayer_count, instlayers);
    assert(!res);
    printf("Available instance layers:\n");
    for (i = 0; i < instlayer_count; ++i) {
      printf("%s\n", instlayers[i].layerName);
    }
    free(instlayers);
  } else {
    printf("No instance layer\n");
  }

  // create instance

  VkApplicationInfo app_info = {.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
                                .pNext = NULL,
                                .pApplicationName = "hello_triangle",
                                .applicationVersion = 0,
                                .pEngineName = "hello_engine",
                                .engineVersion = 0,
                                .apiVersion = VK_API_VERSION};

  VkInstanceCreateInfo inst_info = {
      .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
      .pNext = NULL,
      .flags = 0,
      .pApplicationInfo = &app_info,
      .enabledExtensionCount = instExtensionCount,
      .ppEnabledExtensionNames = instExtensionNames,
      .enabledLayerCount = instLayerCount,
      .ppEnabledLayerNames = instLayerNames};

  VkInstance inst;
  res = vkCreateInstance(&inst_info, NULL, &inst);
  assert(!res);

  // enable debugging

  VkDebugReportCallbackCreateInfoEXT debug_info = {
      .sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT,
      .flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT,
      .pfnCallback = dbgFunc,
      .pUserData = NULL,
      .pNext = NULL};
  VkDebugReportCallbackEXT debugcallback;
  PFN_vkCreateDebugReportCallbackEXT createfn =
      (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(
          inst, "vkCreateDebugReportCallbackEXT");
  assert(createfn);
  res = createfn(inst, &debug_info, NULL, &debugcallback);
  assert(!res);

  // create physical device

  unsigned gpu_count;
  vkEnumeratePhysicalDevices(inst, &gpu_count, NULL);
  assert(gpu_count);

  VkPhysicalDevice *gpus, gpu;
  gpus = malloc(gpu_count * sizeof(VkPhysicalDevice));
  assert(gpus);

  match = 0;
  vkEnumeratePhysicalDevices(inst, &gpu_count, gpus);
  for (i = 0; i < gpu_count; ++i) {
    VkPhysicalDeviceProperties device_props;
    vkGetPhysicalDeviceProperties(gpus[i], &device_props);
    if (device_props.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
      printf("Selected gpu: %s\n", device_props.deviceName);
      gpu = gpus[i];
      match = 1;
      break;
    }
  }
  free(gpus);
  assert(match);

  VkPhysicalDeviceMemoryProperties memprops;
  vkGetPhysicalDeviceMemoryProperties(gpu, &memprops);

  // create window

  struct sys sys;
  assert(create_connection(&sys) == 0);
  create_window(&sys);

  // create surface

  VkXcbSurfaceCreateInfoKHR surf_info = {
      .sType = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR,
      .pNext = NULL,
      .flags = 0,
      .connection = sys.connection,
      .window = sys.window};

  VkSurfaceKHR surf;
  res = vkCreateXcbSurfaceKHR(inst, &surf_info, NULL, &surf);
  assert(!res);

  unsigned qf_prop_count;
  vkGetPhysicalDeviceQueueFamilyProperties(gpu, &qf_prop_count, NULL);

  VkQueueFamilyProperties *qf_props, qf_prop;
  unsigned qf_index;
  qf_props = malloc(qf_prop_count * sizeof(VkQueueFamilyProperties));
  assert(qf_props);

  match = 0;
  vkGetPhysicalDeviceQueueFamilyProperties(gpu, &qf_prop_count, qf_props);
  for (i = 0; i < qf_prop_count; ++i) {
    if (qf_props[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
      VkBool32 supports_present = 0;
      vkGetPhysicalDeviceSurfaceSupportKHR(gpu, i, surf, &supports_present);
      // queue with GRAPHICS_BIT might differ from queue, that supports present
      if (supports_present) {
        printf("Selected queue index: %d\n", i);
        qf_prop = qf_props[i];
        qf_index = i;
        match = 1;
        break;
      }
    }
  }
  free(qf_props);
  assert(match);

  // find device extensions

  unsigned devext_count;
  res = vkEnumerateDeviceExtensionProperties(gpu, NULL, &devext_count, NULL);
  assert(!res);

  if (devext_count > 0) {
    VkExtensionProperties *exts =
        malloc(devext_count * sizeof(VkExtensionProperties));
    assert(exts);
    res = vkEnumerateDeviceExtensionProperties(gpu, NULL, &devext_count, exts);
    assert(!res);
    printf("Available device extenstions:\n");
    for (i = 0; i < devext_count; ++i) {
      printf("%s\n", exts[i].extensionName);
    }
    free(exts);
  } else {
    printf("No device extenstion available\n");
  }

  // create device

  float queue_priorities[1] = {0.0f};
  VkDeviceQueueCreateInfo queue_info = {
      .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
      .pNext = NULL,
      .flags = 0,
      .queueFamilyIndex = qf_index,
      .queueCount = 1,
      .pQueuePriorities = queue_priorities};
  VkDeviceCreateInfo device_info = {
      .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
      .pNext = NULL,
      .flags = 0,
      .queueCreateInfoCount = 1,
      .pQueueCreateInfos = &queue_info,
      .enabledLayerCount = devLayerCount,
      .ppEnabledLayerNames = devLayerNames,
      .enabledExtensionCount = devExtensionCount,
      .ppEnabledExtensionNames = devExtensionNames,
      .pEnabledFeatures = NULL};

  VkDevice device;
  res = vkCreateDevice(gpu, &device_info, NULL, &device);
  assert(!res);

  // get queue

  VkQueue queue;
  vkGetDeviceQueue(device, qf_index, 0, &queue);

  // create swapchain

  const VkSwapchainCreateInfoKHR swapchain_info = {
      .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
      .pNext = NULL,
      .surface = surf,
      .minImageCount = SWAPCHAIN_IMAGES,
      .imageFormat = VK_FORMAT_B8G8R8A8_UNORM,
      .imageColorSpace = VK_COLORSPACE_SRGB_NONLINEAR_KHR,
      .imageExtent =
          {
              .width = WINDOW_W, .height = WINDOW_H,
          },
      .imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
      .preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR,
      .compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
      .imageArrayLayers = 1,
      .imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
      .queueFamilyIndexCount = 0,
      .pQueueFamilyIndices = NULL,
      .presentMode = VK_PRESENT_MODE_FIFO_KHR,
      .oldSwapchain = NULL,
      .clipped = VK_TRUE,
  };
  VkSwapchainKHR swapchain;
  res = vkCreateSwapchainKHR(device, &swapchain_info, NULL, &swapchain);
  assert(!res);

  // get swapchain images

  unsigned image_count;
  vkGetSwapchainImagesKHR(device, swapchain, &image_count, NULL);
  VkImage *images = malloc(image_count * sizeof(VkImage));
  assert(images);
  vkGetSwapchainImagesKHR(device, swapchain, &image_count, images);

  // create views of swapchain images

  VkImageView *views = malloc(SWAPCHAIN_IMAGES * sizeof(VkImageView));
  assert(views);
  for (i = 0; i < SWAPCHAIN_IMAGES; ++i) {
    VkImageViewCreateInfo view_info = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .format = VK_FORMAT_B8G8R8A8_UNORM,
        .components =
            {
                .r = VK_COMPONENT_SWIZZLE_R,
                .g = VK_COMPONENT_SWIZZLE_G,
                .b = VK_COMPONENT_SWIZZLE_B,
                .a = VK_COMPONENT_SWIZZLE_A,
            },
        .subresourceRange = {.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                             .baseMipLevel = 0,
                             .levelCount = 1,
                             .baseArrayLayer = 0,
                             .layerCount = 1},
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .image = images[i]};
    res = vkCreateImageView(device, &view_info, NULL, views + i);
    assert(!res);
  }

  // create renderpass

  VkAttachmentDescription attachments[1] =
      {[0] = {
           .format = VK_FORMAT_B8G8R8A8_UNORM,
           .samples = VK_SAMPLE_COUNT_1_BIT,
           .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
           .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
           .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
           .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
           .initialLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
           .finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
       }};
  VkAttachmentReference color_reference = {
      .attachment = 0, .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
  };
  VkSubpassDescription subpass = {
      .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
      .flags = 0,
      .inputAttachmentCount = 0,
      .pInputAttachments = NULL,
      .colorAttachmentCount = 1,
      .pColorAttachments = &color_reference,
      .pResolveAttachments = NULL,
      .pDepthStencilAttachment = NULL,
      .preserveAttachmentCount = 0,
      .pPreserveAttachments = NULL,
  };
  VkRenderPassCreateInfo renderpass_info = {
      .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
      .pNext = NULL,
      .attachmentCount = 1,
      .pAttachments = attachments,
      .subpassCount = 1,
      .pSubpasses = &subpass,
      .dependencyCount = 0,
      .pDependencies = NULL,
  };
  VkRenderPass renderpass;
  res = vkCreateRenderPass(device, &renderpass_info, NULL, &renderpass);
  assert(!res);

  // create vertex buffer

  VkBufferCreateInfo vbbuf_info = {
      .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
      .pNext = NULL,
      .size = sizeof(vb),
      .usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
      .flags = 0,
  };
  VkBuffer vbbuf;
  res = vkCreateBuffer(device, &vbbuf_info, NULL, &vbbuf);
  assert(!res);

  VkMemoryRequirements mem_reqs;
  vkGetBufferMemoryRequirements(device, vbbuf, &mem_reqs);

  VkMemoryAllocateInfo vbmem_info = {
      .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
      .pNext = NULL,
      .allocationSize = mem_reqs.size,
      .memoryTypeIndex = 0,
  };
  res = memtype_from_properties(&memprops, mem_reqs.memoryTypeBits,
                                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
                                &vbmem_info.memoryTypeIndex);
  assert(res);

  VkDeviceMemory vbmem;
  res = vkAllocateMemory(device, &vbmem_info, NULL, &vbmem);
  assert(!res);

  void *data;
  res = vkMapMemory(device, vbmem, 0, vbmem_info.allocationSize, 0, &data);
  assert(!res);
  memcpy(data, vb, sizeof(vb));
  vkUnmapMemory(device, vbmem);
  res = vkBindBufferMemory(device, vbbuf, vbmem, 0);
  assert(!res);

  // load shaders

  size_t vs_codesiz;
  void *vs_code = read_spv("vs.spv", &vs_codesiz);
  assert(vs_code);
  VkShaderModuleCreateInfo vs_info = {
      .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
      .pNext = NULL,
      .codeSize = vs_codesiz,
      .pCode = vs_code,
      .flags = 0};
  VkShaderModule vs;
  res = vkCreateShaderModule(device, &vs_info, NULL, &vs);
  free(vs_code);
  assert(!res);

  size_t fs_codesiz;
  void *fs_code = read_spv("fs.spv", &fs_codesiz);
  assert(fs_code);
  VkShaderModuleCreateInfo fs_info = {
      .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
      .pNext = NULL,
      .codeSize = fs_codesiz,
      .pCode = fs_code,
      .flags = 0};
  VkShaderModule fs;
  res = vkCreateShaderModule(device, &fs_info, NULL, &fs);
  free(fs_code);
  assert(!res);

  // create pipeline

  VkVertexInputBindingDescription vi_bindings = {
      .binding = 0,
      .stride = sizeof(vb[0]),
      .inputRate = VK_VERTEX_INPUT_RATE_VERTEX};

  VkVertexInputAttributeDescription vi_attrs[1] = {
          [0] = {.binding = 0,
                 .location = 0,
                 .format = VK_FORMAT_R32G32B32_SFLOAT,
                 .offset = 0},
  };
  VkPipelineVertexInputStateCreateInfo vi = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
      .pNext = NULL,
      .vertexBindingDescriptionCount = 1,
      .pVertexBindingDescriptions = &vi_bindings,
      .vertexAttributeDescriptionCount = 1,
      .pVertexAttributeDescriptions = vi_attrs};
  VkPipelineInputAssemblyStateCreateInfo ia = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
      .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST};
  VkPipelineRasterizationStateCreateInfo rs = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
      .polygonMode = VK_POLYGON_MODE_FILL,
      .cullMode = VK_CULL_MODE_BACK_BIT,
      .frontFace = VK_FRONT_FACE_CLOCKWISE,
      .depthClampEnable = VK_FALSE,
      .rasterizerDiscardEnable = VK_FALSE,
      .depthBiasEnable = VK_FALSE};
  VkPipelineColorBlendAttachmentState att_state = {.colorWriteMask = 0xf,
                                                   .blendEnable = VK_FALSE};
  VkPipelineColorBlendStateCreateInfo cb = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
      .attachmentCount = 1,
      .pAttachments = &att_state};
  VkPipelineViewportStateCreateInfo vp = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
      .viewportCount = 1,
      .scissorCount = 1};
  VkPipelineMultisampleStateCreateInfo ms = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
      .pSampleMask = NULL,
      .rasterizationSamples = VK_SAMPLE_COUNT_1_BIT};
  VkPipelineShaderStageCreateInfo shaderStages[2] =
      {[0] = {.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
              .stage = VK_SHADER_STAGE_VERTEX_BIT,
              .module = vs,
              .pName = "main"},
       [1] = {.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
              .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
              .module = fs,
              .pName = "main"}};
  VkPipelineLayoutCreateInfo pipelayout_info = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
      .pNext = NULL,
      .setLayoutCount = 0,
      .pSetLayouts = NULL,
  };
  VkPipelineLayout pipelayout;
  res = vkCreatePipelineLayout(device, &pipelayout_info, NULL, &pipelayout);
  assert(!res);

  VkDynamicState dynamicStateEnables[VK_DYNAMIC_STATE_RANGE_SIZE] = {};
  VkPipelineDynamicStateCreateInfo dynamicState = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
      .pDynamicStates = dynamicStateEnables,
      .dynamicStateCount = 2};
  dynamicStateEnables[0] = VK_DYNAMIC_STATE_VIEWPORT;
  dynamicStateEnables[1] = VK_DYNAMIC_STATE_SCISSOR;

  VkPipelineCacheCreateInfo pipecache_info = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO};
  VkPipelineCache pipecache;
  res = vkCreatePipelineCache(device, &pipecache_info, NULL, &pipecache);
  assert(!res);

  VkGraphicsPipelineCreateInfo pipe_info = {
      .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
      .layout = pipelayout,
      .pVertexInputState = &vi,
      .pInputAssemblyState = &ia,
      .pRasterizationState = &rs,
      .pColorBlendState = &cb,
      .pMultisampleState = &ms,
      .pViewportState = &vp,
      .pDepthStencilState = NULL,
      .stageCount = 2,
      .pStages = shaderStages,
      .renderPass = renderpass,
      .pDynamicState = &dynamicState};

  VkPipeline pipe;
  res =
      vkCreateGraphicsPipelines(device, pipecache, 1, &pipe_info, NULL, &pipe);
  assert(!res);

  vkDestroyPipelineCache(device, pipecache, NULL);
  vkDestroyShaderModule(device, fs, NULL);
  vkDestroyShaderModule(device, vs, NULL);

  // create command pool

  VkCommandPoolCreateInfo cmdpool_info = {
      .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
      .pNext = NULL,
      .queueFamilyIndex = qf_index,
      .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
  };
  VkCommandPool cmdpool;
  res = vkCreateCommandPool(device, &cmdpool_info, NULL, &cmdpool);
  assert(!res);

  // allocate command buffer

  const VkCommandBufferAllocateInfo cmdbuf_info = {
      .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
      .pNext = NULL,
      .commandPool = cmdpool,
      .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
      .commandBufferCount = 1,
  };
  VkCommandBuffer cmdbuf;
  res = vkAllocateCommandBuffers(device, &cmdbuf_info, &cmdbuf);
  assert(!res);

  // create framebuffers

  VkFramebuffer *framebuffers =
      (VkFramebuffer *)malloc(SWAPCHAIN_IMAGES * sizeof(VkFramebuffer));
  assert(framebuffers);
  for (i = 0; i < SWAPCHAIN_IMAGES; i++) {
    VkFramebufferCreateInfo framebuf_info = {
        .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
        .pNext = NULL,
        .renderPass = renderpass,
        .attachmentCount = 1,
        .pAttachments = &views[i],
        .width = WINDOW_W,
        .height = WINDOW_H,
        .layers = 1,
    };
    res = vkCreateFramebuffer(device, &framebuf_info, NULL, &framebuffers[i]);
    assert(!res);
  }

  // drawing loop

  xcb_flush(sys.connection);
  while (1) {

    // process user input

    xcb_generic_event_t *event;
    event = xcb_poll_for_event(sys.connection);
    int should_close = 0;
    if (event) {
      switch (event->response_type & 0x7f) {
      case XCB_EXPOSE:
        break;
      case XCB_CLIENT_MESSAGE:
        if ((*(xcb_client_message_event_t *)event).data.data32[0] ==
            (*sys.atom_wm_delete_window).atom) {
          should_close = 1;
        }
        break;
      case XCB_KEY_RELEASE: {
        const xcb_key_release_event_t *key =
            (const xcb_key_release_event_t *)event;
        if (key->detail == 0x9)
          should_close = 1;
      } break;
      case XCB_DESTROY_NOTIFY:
        should_close = 1;
        break;
      case XCB_CONFIGURE_NOTIFY:
        break;
      default:
        break;
      }
      free(event);
    }
    if (should_close) {
      break;
    }

   // create sem.

    VkSemaphoreCreateInfo presentCompleteSemaphoreCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
    };
    VkSemaphore presentCompleteSemaphore;
    res = vkCreateSemaphore(device, &presentCompleteSemaphoreCreateInfo, NULL,
                            &presentCompleteSemaphore);
    assert(!res);

    // request next image

    unsigned current_buffer;
    res = vkAcquireNextImageKHR(device, swapchain, UINT64_MAX,
                                presentCompleteSemaphore, (VkFence)0,
                                &current_buffer);
    if (res == VK_ERROR_OUT_OF_DATE_KHR) {
      // demo->swapchain is out of date (e.g. the window was resized) and
      // must be recreated:
      assert(0);
      break;
    } else if (res == VK_SUBOPTIMAL_KHR) {
      // demo->swapchain is not as optimal as it could be, but the platform's
      // presentation engine will still present the image correctly.
    } else {
      assert(!res);
    }

    // start commands

    VkCommandBufferInheritanceInfo cmd_buf_hinfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO,
        .pNext = NULL,
        .renderPass = VK_NULL_HANDLE,
        .subpass = 0,
        .framebuffer = VK_NULL_HANDLE,
        .occlusionQueryEnable = VK_FALSE,
        .queryFlags = 0,
        .pipelineStatistics = 0,
    };
    VkCommandBufferBeginInfo cmd_buf_info = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .pNext = NULL,
        .flags = 0,
        .pInheritanceInfo = &cmd_buf_hinfo,
    };
    VkClearValue clear_values[1] = {
            [0] = {.color.float32 = {0.2f, 0.2f, 0.2f, 0.2f}},
    };
    VkRenderPassBeginInfo rp_begin = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
        .pNext = NULL,
        .renderPass = renderpass,
        .framebuffer = framebuffers[current_buffer],
        .renderArea.offset.x = 0,
        .renderArea.offset.y = 0,
        .renderArea.extent.width = WINDOW_W,
        .renderArea.extent.height = WINDOW_H,
        .clearValueCount = 1,
        .pClearValues = clear_values,
    };
    res = vkBeginCommandBuffer(cmdbuf, &cmd_buf_info);
    assert(!res);

    vkCmdBeginRenderPass(cmdbuf, &rp_begin, VK_SUBPASS_CONTENTS_INLINE);
    vkCmdBindPipeline(cmdbuf, VK_PIPELINE_BIND_POINT_GRAPHICS, pipe);
    // vkCmdBindDescriptorSets(demo->draw_cmd, VK_PIPELINE_BIND_POINT_GRAPHICS,
    //                        demo->pipeline_layout, 0, 1, &demo->desc_set, 0,
    //                        NULL);

    VkViewport viewport = {
        .height = (float)WINDOW_W,
        .width = (float)WINDOW_H,
        .minDepth = (float)0.0f,
        .maxDepth = (float)1.0f,
    };
    vkCmdSetViewport(cmdbuf, 0, 1, &viewport);

    VkRect2D scissor = {
        .extent = {.width = WINDOW_W, .height = WINDOW_H}, .offset = {.x = 0, .y = 0},
    };
    vkCmdSetScissor(cmdbuf, 0, 1, &scissor);

    VkDeviceSize offsets[1] = {0};
    vkCmdBindVertexBuffers(cmdbuf, 0, 1, &vbbuf, offsets);

    vkCmdDraw(cmdbuf, 3, 1, 0, 0);
    vkCmdEndRenderPass(cmdbuf);

    VkImageMemoryBarrier prePresentBarrier = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .pNext = NULL,
        .srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
        .dstAccessMask = VK_ACCESS_MEMORY_READ_BIT,
        .oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        .newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .subresourceRange = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1}};

    prePresentBarrier.image = images[current_buffer];
    VkImageMemoryBarrier *pmemory_barrier = &prePresentBarrier;
    vkCmdPipelineBarrier(cmdbuf, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
                         VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, NULL, 0,
                         NULL, 1, pmemory_barrier);

    res = vkEndCommandBuffer(cmdbuf);
    assert(!res);

    VkFence nullFence = VK_NULL_HANDLE;
    VkPipelineStageFlags pipe_stage_flags =
        VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
    VkSubmitInfo submit_info = {.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
                                .pNext = NULL,
                                .waitSemaphoreCount = 1,
                                .pWaitSemaphores = &presentCompleteSemaphore,
                                .pWaitDstStageMask = &pipe_stage_flags,
                                .commandBufferCount = 1,
                                .pCommandBuffers = &cmdbuf,
                                .signalSemaphoreCount = 0,
                                .pSignalSemaphores = NULL};

    // submit commands

    res = vkQueueSubmit(queue, 1, &submit_info, nullFence);
    assert(!res);

    VkPresentInfoKHR present = {
        .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
        .pNext = NULL,
        .swapchainCount = 1,
        .pSwapchains = &swapchain,
        .pImageIndices = &current_buffer,
    };

    res = vkQueuePresentKHR(queue, &present);
    if (res == VK_ERROR_OUT_OF_DATE_KHR) {
      // demo->swapchain is out of date (e.g. the window was resized) and
      assert(0);
    } else if (res == VK_SUBOPTIMAL_KHR) {
      // demo->swapchain is not as optimal as it could be, but the platform's
      // presentation engine will still present the image correctly.
    } else {
      assert(!res);
    }

    res = vkQueueWaitIdle(queue);
    assert(!res);

    vkDestroySemaphore(device, presentCompleteSemaphore, NULL);

    vkDeviceWaitIdle(device);
  }

  // clean up

  for (i = 0; i < SWAPCHAIN_IMAGES; ++i) {
    vkDestroyFramebuffer(device, framebuffers[i], NULL);
  }
  vkFreeCommandBuffers(device, cmdpool, 1, &cmdbuf);
  vkDestroyCommandPool(device, cmdpool, NULL);
  vkDestroyPipeline(device, pipe, NULL);
  vkDestroyPipelineLayout(device, pipelayout, NULL);
  vkFreeMemory(device, vbmem, NULL);
  vkDestroyBuffer(device, vbbuf, NULL);
  vkDestroyRenderPass(device, renderpass, NULL);
  for (i = 0; i < SWAPCHAIN_IMAGES; ++i) {
    vkDestroyImageView(device, views[i], NULL);
  }
  vkDestroySwapchainKHR(device, swapchain, NULL);
  vkDestroyDevice(device, NULL);
  vkDestroySurfaceKHR(inst, surf, NULL);
  destroy_window(&sys);
  destroy_connection(&sys);

  PFN_vkDestroyDebugReportCallbackEXT destroyfn =
      (PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(
          inst, "vkDestroyDebugReportCallbackEXT");
  assert(destroyfn);
  destroyfn(inst, debugcallback, NULL);

  vkDestroyInstance(inst, NULL);

  return 0;
}
