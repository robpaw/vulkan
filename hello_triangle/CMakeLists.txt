cmake_minimum_required(VERSION 2.8)

SET(EXEFILE hello_triangle)

project(${EXEFILE})

add_executable(${EXEFILE} main.c)
target_link_libraries(${EXEFILE} vulkan xcb)

add_custom_command(TARGET ${EXEFILE} 
    PRE_BUILD 
    COMMAND glslangValidator -s -V -o ${CMAKE_BINARY_DIR}/vs.spv ${CMAKE_SOURCE_DIR}/sh.vert 
    COMMAND glslangValidator -s -V -o ${CMAKE_BINARY_DIR}/fs.spv ${CMAKE_SOURCE_DIR}/sh.frag 
    WORKING_DIRECTORY ${CMAKE_BINARY_DIR} 
    COMMENT "Compiling shaders..." VERBATIM )

