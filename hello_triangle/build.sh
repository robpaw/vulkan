#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
[[ -d ${DIR}/build ]] && rm -rf ${DIR}/build
mkdir -p ${DIR}/build && cd ${DIR}/build && cmake ../. && make && ./hello_triangle
